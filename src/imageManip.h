#ifndef MANIP_H
#define MANIP_H

#include "ppm_io.h"
unsigned char saturation (double); 
void swap (Image *);
void bright (Image*, double); 
void invert (Image *);
void grayscale (Image *);
Image * crop (Image *, int, int, int, int);
double sq (double);
Image * blur (Image *, double);
void new_pixel (Image * im, Image * new_im, int pix_i, int pix_j, int size_gaus, double gaus[size_gaus][size_gaus]);
Image * edge (Image *, double, double); 
#endif
