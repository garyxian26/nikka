#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "op_select.h"
#include "ppm_io.h"
#include "imageManip.h"

int op_select (int argc, char **argv) {
  //Check for error 1, i.e. Failed to supply
  //input filename or output filename, or both
  //by checking the number of command line arguments
  if (argc < 3) {
    printf("Failure to supply input filename, output filename, or both!\n"); 
    return 1; 
  }

  //Check for error 4, i.e. no operation name was specified
  //by checking if there is a fourth command line argument
  if (argc == 3) {
    printf("No operation name was specified!\n");
    return 4; 
  }

  //Obtain the input file name and output file name from the user
  char *input_filename = argv[1];
  char *output_filename = argv[2];

  //Open input file and check for error 2
  FILE* input = fopen(input_filename, "rb");
  if (!input) {
    printf("Specified input file could not be opened.\n");
    return 2; 
  }

  //Create Image struct im to read the input file
  Image * im = read_ppm(input);
  //Check for error 3
  if (!im) {
    printf("Specified input file is not a properly-formatteed PPM file, or reading input somehow fails.\n");
    return 3; 
  }

  //Close input file
  fclose(input);
  
  //Obtain the operation name
  char *operation = argv[3];

  //lower case every letter
  int i = 0;
  while (operation[i]) {
    operation[i] = tolower(operation[i]);
    i++; 
  }

  //Select Operations
  if (strcmp(operation, "swap")==0) {
    if (argc!=4) {
      printf("Incorrect number of arguments\n");
      return 5;
    }
    
    swap(im);
  }
  else if (strcmp(operation, "bright")==0) {
    if (argc!=5) {
      printf("Incorrect number of arguments\n");
      return 5; 
    }
    
    double amount = atof(argv[4]);
    bright(im, amount);
  }
  else if (strcmp(operation, "invert")==0) {
    if (argc!=4) {
      printf("Incorrect number of arguments\n");
      return 5;
    }
    
    invert(im);
  }
  else if (strcmp(operation, "gray")==0) {
    if (argc!=4) {
      printf("Incorrect number of arguments\n");
      return 5;
    }
    grayscale(im);
  }

  else if (strcmp(operation, "crop")==0) {
    if (argc!=8) {
      printf("Incorrect number of arguments\n");
      return 5;
    }
    
    int top_col = atoi(argv[4]);
    int top_row = atoi(argv[5]);
    int bot_col = atoi(argv[6]);
    int bot_row = atoi(argv[7]);

    if (bot_row > im->rows || bot_col > im->cols || top_col > bot_col || top_row > bot_row || top_col < 0 || top_row < 0 || bot_col < 0 || bot_row < 0 || (top_row+bot_row+top_col+bot_col)==0 || bot_row == 0 || bot_col == 0) {
      printf("Arguments for the specified operation were out of range for the given input image, or otherwise senseless.\n");
      return 6; 
    }
    
    im = crop(im, top_col, top_row, bot_col, bot_row);
  }

  else if (strcmp(operation, "blur")==0) {
    if (argc!=5) {
      printf("Incorrect number of arguments\n");
      return 5;
    }
    double sigma = atof(argv[4]);
    im = blur(im, sigma);
  }

  else if (strcmp(operation, "edges")==0) {
    if (argc!=6) {
      printf("Incorrect number of arguments\n");
      return 5;
    }
    double sigma = atof(argv[4]);
    double threshold = atof(argv[5]);
    im = edge(im, sigma, threshold);
  }
  
  else {
    printf("Operation name specified was invalid\n");
    free(im->data);
    free(im); 
    return 4; 
  }

  //Open output file and check for error 7                                                                                                    
  FILE* output = fopen(output_filename, "wb");
  if (!output) {
    printf("Specified output file could not be opened for writing.\n");
    return 7;
  }

  //Write the new image to the ouput file
  write_ppm(output, im); 
  
  //Free dynamically allocated memory
  free(im->data);
  free(im);

  //Close out file
  fclose(output);
  
  return 0; 
}
