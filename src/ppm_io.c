
// __Add your name and JHED above__
// ppm_io.c
// 601.220, Spring 2019
// Starter code for midterm project - feel free to edit/add to this file

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "ppm_io.h"



/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp) {

  // check that fp is not NULL
  assert(fp);

  // check if the file is P6
  char file_type[2];
  fscanf(fp, "%c", &file_type[0]);
  fscanf(fp, "%c", &file_type[1]);
  if (!(file_type[0] == 'P' && file_type[1] == '6')) {
    printf("Wrong file type!");
    return NULL; 
  }
  
  //Skip newline character
  fgetc(fp);

  //Check for comments
  char c = fgetc(fp);
  if (c == '#') {
    while (c!= '\n') {
      c = fgetc(fp); 
    }
  }

  //If c is not #, then we are encountering the dimensions
  //Therefore, we need to reread as an int
  //ungetc pushes the character back so that it's available
  //for next read operation
  ungetc(c, fp); 

  //Read dimension
  int width; 
  int height; 

  //Read the width(columns) of the image
  int s = fscanf(fp, "%d", &width); 
  if (s!=1) {
    printf("Error reading number of rows\n");
    return NULL; 
  }
  
  //Read the height(rows) of the image
  s = fscanf(fp, "%d", &height);
  if (s!=1) {
    printf("Error reading number of columns\n");
    return NULL;
  }

  //Skip newline character
  fgetc(fp);

  //Check for maximum color value
  int maxval; 
  s = fscanf(fp, "%d", &maxval);
  if ((s!=1) | (maxval!=255)) {
    printf("Error reading max color value\n");
    return NULL; 
  }

  fgetc(fp);

  //Create an image struct
  Image * im;
  im = (Image *) malloc(sizeof(Image));
  if(!im) {
    fprintf(stderr, "Image allocation failed!\n");
    return NULL;
  }

  //Create the pixel arrage
  im->data = (Pixel *) malloc(sizeof(Pixel) * width * height);
  
  im->rows = height;
  im->cols = width;
  
  //Read into the array
  int num_pixels_read = fread(im->data, sizeof(Pixel), width*height,fp);

  if (num_pixels_read != im->rows * im->cols) {
    fprintf(stderr, "Pixel data failed to read properly!\n"); 
  }
  
  return im; 
  
}


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp); 

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->cols * im->rows, fp);

  if (num_pixels_written != im->cols * im->rows) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }

  return num_pixels_written;
}

