//This is the main program

#include <stdio.h>
#include "op_select.h"
#include "ppm_io.h"
#include "imageManip.h"

int main(int argc, char **argv) {
  op_select(argc, &*argv); 
  return 0; 
}
