#ifndef OP_SELECT_H
#define OP_SELECT_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppm_io.h"

int op_select(int, char**);
#endif
