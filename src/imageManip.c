//Implementations of all image manipulation algorithms

#include "imageManip.h"
#include "ppm_io.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>

#define M_PI 3.14159265358979323846

//Function to check if number overflows or underflows 0-255
unsigned char saturation (double num) {
  if (num>255) {
    return (unsigned char) 255;
  } else if (num<0) {
    return (unsigned char) 0;
  } else {
    return (unsigned char) num;
  }
}
      	       	
void swap (Image * im) {
  for (int i=0; i<im->rows*im->cols; i++) {
    unsigned char old_red = im->data[i].r;
    unsigned char old_green = im->data[i].g;
    unsigned char old_blue = im->data[i].b;
    im->data[i].r = old_green;
    im->data[i].g = old_blue;
    im->data[i].b = old_red;
  }
}

//Function to brighten color channel
void bright (Image * im, double amount) {
  for (int i=0; i<im->rows*im->cols; i++) {
    im->data[i].r = saturation(im->data[i].r+amount);
    im->data[i].g = saturation(im->data[i].g+amount);
    im->data[i].b = saturation(im->data[i].b+amount);
  }
}



//inverts the rgb values of each pixel
void invert (Image * im) {
  for (int i=0; i<im->rows*im->cols; i++) {
    im->data[i].r = 255 - im->data[i].r; 
    im->data[i].g = 255 - im->data[i].g; 
    im->data[i].b = 255 - im->data[i].b; 
  }
}


//uses NTSC standard formula for intensity to set each rgb value equal to total intensity
void grayscale(Image * im) {
  for (int i=0; i<im->rows*im->cols; i++) {
    int intensity = 0.30*(im->data[i].r) + 0.59*(im->data[i].g) + 0.11*(im->data[i].b);
    im->data[i].b = intensity;
    im->data[i].r = intensity;
    im->data[i].g = intensity;
  }
}
 
//Function to crop image according to user given column and row values
Image * crop (Image * im, int tcol, int trow, int bcol, int brow) {

    //create new pointer to new Image struct
    Image * new_im = malloc(sizeof(Image));
    new_im->rows = brow - trow;
    new_im->cols = bcol - tcol;
    new_im->data = malloc(sizeof(Pixel)*new_im->cols*new_im->rows);

    if (new_im->rows == 1 && new_im->cols == 1) {
      new_im->data[0] = im->data[0];
      return new_im;
    }

    //counter for new array
    int counter = 0;
    //tracker for old array
    int start = (trow-1)*(im->cols) + tcol;

    for (int i = 0; i < new_im->rows; i++) {
      for (int j = 0; j < new_im->cols; j++) {
        new_im->data[counter] = im->data[start];
        counter++;
        start++;
      }
      start=start+im->cols-new_im->cols;
    }
    free ((*im).data);
    free (im);
    return new_im;
}	    		

double sq (double num) {
  return num * num;
}

Image * blur (Image * im, double sigma){
  //set N
  int N = sigma * 10;
  if(N % 2 == 0){
    N++;
  }

  //Create the Gaussian filter matrix
  double gaus[N][N];

  //Fill the matrix with values      
  for(int i = -N/2; i <= N/2; i++){
    for(int j = -N/2; j <= N/2; j++){
      gaus[i + N/2][j + N/2] = (1.0 / (2.0 * M_PI * sq(sigma))) * exp( -(sq(i) + sq(j)) / (2 * sq(sigma)));
    }
  }

  Image * new_im = malloc(sizeof(Image));
  new_im->cols = im->cols;
  new_im->rows = im->rows;
  new_im->data = malloc(sizeof(Pixel)*new_im->cols*new_im->rows);
  
  //Iterate through pixel array to update pixels
  for (int i = 0; i < im->rows; i++) {
    for (int j = 0; j < im->cols; j++) {
      new_pixel(im, new_im, i, j, N, gaus);
    }
  }
  free ((*im).data);
  free (im);
  return new_im; 
}

void new_pixel(Image * im, Image * new_im, int pix_i, int pix_j, int size_gaus, double gaus[size_gaus][size_gaus]) {
  int N = size_gaus; 
  
  int row_start = N/2 - pix_i;
  if (row_start < 0) {
    row_start = 0; 
  }
  
  int col_start = N/2 - pix_j;
  if (col_start < 0) {
    col_start = 0; 
  }
  
  int row_end = N/2 - (im->rows-pix_i-1);
  if (row_end < 0) {
    row_end = 0; 
  }
  
  int col_end = N/2 - (im->cols-pix_j-1);
  if (col_end < 0) {
    col_end = 0; 
  }

  double new_r = 0;
  double new_g = 0;
  double new_b = 0;
  double weight = 0;

  int filter_j = pix_j - N/2 + col_start;
  for (int i = col_start; i <= N/2 + (N/2 - col_end); i++) {
    int filter_i = pix_i - N/2 + row_start;
    for (int j = row_start; j <= N/2 + (N/2 - row_end); j++){
      new_r += im->data[filter_i*im->cols+filter_j].r * gaus[i][j];
      new_g += im->data[filter_i*im->cols+filter_j].g *	gaus[i][j];
      new_b += im->data[filter_i*im->cols+filter_j].b *	gaus[i][j];
      
      weight += gaus[i][j];

      filter_i++; 
    }
    filter_j++; 
  }

  new_im->data[pix_i*im->cols+pix_j].r = (unsigned char) (new_r/weight);
  new_im->data[pix_i*im->cols+pix_j].g = (unsigned char) (new_g/weight);
  new_im->data[pix_i*im->cols+pix_j].b = (unsigned char) (new_b/weight);
  
}

Image * edge (Image * im, double sigma, double threshold) {
  grayscale(im);
  im = blur(im, sigma);

  Image * new_im = malloc(sizeof(Image));
  new_im->cols = im->cols;
  new_im->rows = im->rows;
  new_im->data = malloc(sizeof(Pixel)*im->cols*im->rows);

  double delta_x_r = 0;
  double delta_y_r = 0;
  double delta_r = 0;

  for (int i = 0; i < new_im->rows; i++) {
    for (int j = 0; j < new_im->cols; j++) {
      //Check if pixel is on the boundary
      if (!(i==0 || j==0 || i==(new_im->rows-1) || j==(new_im->cols-1))) {
        delta_x_r = (im->data[(i+1)*im->cols+j].r - im->data[(i-1)*im->cols+j].r)/2;
        delta_y_r = (im->data[(i)*im->cols+j+1].r - im->data[(i)*im->cols+j-1].r)/2;
        delta_r = sqrt(sq(delta_x_r)+sq(delta_y_r));

        if (delta_r > threshold) {
          new_im->data[i*new_im->cols+j].r = 0;
          new_im->data[i*new_im->cols+j].g = 0;
          new_im->data[i*new_im->cols+j].b = 0;
        } else {
          new_im->data[i*new_im->cols+j].r = 255;
          new_im->data[i*new_im->cols+j].g = 255;
          new_im->data[i*new_im->cols+j].b = 255;
        }
      }
    }
  }
  free(im->data);
  free(im);
  return new_im;  
}
