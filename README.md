This image-manipulation program involves reading and altering pixels in binary images using C.

Possible functions include:
Swapping color
Adjusting brightness
Inverting color
Converting to grayscale
Cropping
Blurring
Edge Detection
